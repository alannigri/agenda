package com.company;

public class Main {

    public static void main(String[] args) {
        IO io = new IO();
        int qtde = io.quantidadePessoas();
        Pessoa pessoa;
        Agenda agenda = new Agenda();
        for(int i=0;i<qtde;i++){
            pessoa = new Pessoa(io.setNomePessoa(), io.setEmailPessoa(), io.setTelefonePessoa());
            agenda.adicionarPessoa(pessoa);
        }
        agenda.mostrarLista();
        int ret = io.menu();
        if(ret == 1){
            agenda.remover(io.buscar());
            agenda.mostrarLista();
        } else{
            agenda.imprimir(io.buscar());
        }
    }
}
