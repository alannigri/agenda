package com.company;

import java.util.HashMap;

public class Agenda {
    private int quantidade;
    private HashMap<Integer, Pessoa> agenda = new HashMap<>();
    Pessoa pessoa;

    public Agenda() {
    }

    public void adicionarPessoa(Pessoa pessoa) {
        agenda.put(agenda.size(), pessoa);

    }

    public void mostrarLista() {
        for (HashMap.Entry<Integer, Pessoa> i : agenda.entrySet()) {
            Pessoa p = i.getValue();
            System.out.println(p.getNome() + ";" + p.getEmail() + ";" + p.getTelefone());
        }
    }

    public void remover(String buscar) {
        for (HashMap.Entry<Integer, Pessoa> i : agenda.entrySet()) {
            Pessoa p = i.getValue();
            if (p.getEmail().equals(buscar)) {
                agenda.remove(i.getKey());
                break;
            } else if (p.getTelefone().equals(buscar)) {
                agenda.remove(i.getKey());
                break;
            }
        }
    }

    public void imprimir(String buscar) {
        for (HashMap.Entry<Integer, Pessoa> i : agenda.entrySet()) {
            Pessoa p = i.getValue();
            if ((p.getEmail().equals(buscar)) || (p.getTelefone().equals(buscar))) {
                System.out.println(p.getNome() + ";" + p.getEmail() + ";" + p.getTelefone());
                break;
            }
        }
    }
}
