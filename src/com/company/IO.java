package com.company;

import java.util.Scanner;

public class IO {
    Scanner scanner = new Scanner(System.in);

    public int quantidadePessoas(){
        System.out.println("Quantas pessoas deseja armazenar?");
        int qtde = scanner.nextInt();
        return qtde;
    }

    public String setNomePessoa(){
        System.out.println("Nome: ");
        String nome = scanner.next();
        return nome;
    }
    public String setEmailPessoa(){
        System.out.println("Email: ");
        String email = scanner.next();
        return email;
    }
    public String setTelefonePessoa(){
        System.out.println("Telefone: ");
        String tel = scanner.next();
        return tel;
    }

    public int menu(){
        System.out.println("Digite 1 para remover ou 2 para imprimir");
        int retorno = scanner.nextInt();
        return retorno;
    }

    public static void naoExiste(){
        System.out.println("Não encontrado");
    }

    public String buscar(){
        System.out.println("Digite email ou telefone:");
        String resultado = scanner.next();
        return resultado;
    }
}
